package com.test.testproject.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.an.customfontview.CustomEditText;
import com.an.customfontview.CustomTextView;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.test.testproject.R;
import com.test.testproject.adapter.EmailListAdapter;
import com.test.testproject.model.EmailListModelClass;
import com.test.testproject.network.ApiClient;
import com.test.testproject.network.CheckConnection;
import com.test.testproject.utils.BaseActivity;
import com.test.testproject.utils.Constants;
import com.test.testproject.utils.ValidationUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;

import androidx.appcompat.app.AppCompatDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {

    private CustomTextView addEmailButton;
    private String TAG = "MainActivity";
    RecyclerView emailListRecyclerView;
    ArrayList<EmailListModelClass> globalEmailList;
    EmailListAdapter emailListAdapter;
    RelativeLayout progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initListener();
        showEmailList();
    }

    private void showEmailList() {
        progress.setVisibility(View.VISIBLE);
        if (CheckConnection.isConnectingToInternet(this)) {
            ApiClient.getInstance(this).getEmailList().enqueue(new Callback<ArrayList<EmailListModelClass>>() {
                public void onResponse(Call<ArrayList<EmailListModelClass>> call, Response<ArrayList<EmailListModelClass>> response) {
                    try {
                        progress.setVisibility(View.GONE);
                        if (response.code() == 200) {
                            globalEmailList = response.body();
                            setdataInAdapter(globalEmailList);

                        } else if (response.code() == 400 || response.code() == 401 || response.code() == 405) {
                            Gson gson = new Gson();
                            Type type = (new TypeToken<EmailListModelClass>() {
                            }).getType();
                            EmailListModelClass var5 = gson.fromJson(response.errorBody().charStream(), type);
                        }
                    } catch (JsonIOException e) {
                        e.printStackTrace();
                    } catch (JsonSyntaxException e) {
                        e.printStackTrace();
                    }

                }

                public void onFailure(Call<ArrayList<EmailListModelClass>> call, Throwable t) {
                    Log.e(TAG, Arrays.toString(t.getStackTrace()));
                    progress.setVisibility(View.GONE);
                }
            });
        }
    }


    private void setdataInAdapter(final ArrayList<EmailListModelClass> emailList) {
        emailListAdapter = new EmailListAdapter(emailList, new EmailListAdapter.OnEditDeleteClickListner() {
            @Override
            public void onEdit(final EmailListModelClass emailListModelClass) {
                showAlertDialog("Update Email", "UPDATE", false, new CustomAlertDialogButtonsCallbackListener() {
                    @Override
                    public void onPositiveButtonClick(AppCompatDialog dialog, String email) {


                        if (ValidationUtils.emailValidation((CustomEditText)dialog.findViewById(R.id.edittext_email))) {
                            dialog.dismiss();
                            updateEmailToServer(emailListModelClass.getIdtableEmail(), email);

                        }
                    }

                    @Override
                    public void onNegativeButtonClick(AppCompatDialog dialog) {
                        dialog.dismiss();
                    }
                });
            }

            @Override
            public void onDelete(final EmailListModelClass emailListModelClass) {
                showDeleteAlertDialog(emailListModelClass.getTableEmailEmailAddress(), false, new CustomAlertDialogButtonsCallbackListener() {
                    @Override
                    public void onPositiveButtonClick(AppCompatDialog dialog, String email) {
                        dialog.dismiss();
                        deleteEmailAddress(emailListModelClass);
                    }

                    @Override
                    public void onNegativeButtonClick(AppCompatDialog dialog) {
                        dialog.dismiss();
                    }
                });

            }
        });
        emailListRecyclerView.setAdapter(emailListAdapter);
    }

    private void deleteEmailAddress(final EmailListModelClass emailListModelClass) {
        progress.setVisibility(View.VISIBLE);
        Log.d(TAG+"Deleted Email",""+emailListModelClass.getIdtableEmail());
        ApiClient.getInstance(this).deleteEmail(emailListModelClass.getIdtableEmail()).enqueue(new Callback<Boolean>() {
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                try {
                    Log.d(TAG+"Response",""+response);
                    progress.setVisibility(View.GONE);
                    if (response.code() == 200) {
                        deleteRecordAfterSuccess(emailListModelClass, response.body());

                    } else if (response.code() == 400 || response.code() == 401 || response.code() == 405) {
                        Gson gson = new Gson();
                        Type type = (new TypeToken<EmailListModelClass>() {
                        }).getType();
                        EmailListModelClass var5 = gson.fromJson(response.errorBody().charStream(), type);
                    }
                } catch (JsonIOException e) {
                    e.printStackTrace();
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }

            }

            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e(TAG, Arrays.toString(t.getStackTrace()));
                progress.setVisibility(View.GONE);
            }
        });

    }

    private void deleteRecordAfterSuccess(EmailListModelClass emailListModelClass, Boolean body) {
        if (body) {
            for (int i = 0; i < globalEmailList.size(); i++) {
                if (globalEmailList.get(i).getIdtableEmail() == emailListModelClass.getIdtableEmail()) {

                    globalEmailList.remove(emailListModelClass);
                    emailListAdapter.notifyItemRemoved(i);
                    return;
                }
            }
        }
    }

    private void updateEmailToServer(int UniqueIdentifier, String email) {
        progress.setVisibility(View.VISIBLE);
        JSONObject jObject = new JSONObject();
        try {
            jObject.put("tableEmailEmailAddress", email);
            jObject.put("tableEmailValidate", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG+"Request",jObject.toString());
        RequestBody requestBody = RequestBody.create(MediaType.parse(Constants.APPLICATION_JSON), jObject.toString());
        ApiClient.getInstance(this).updateEmail(UniqueIdentifier, requestBody).enqueue(new Callback<EmailListModelClass>() {
            public void onResponse(Call<EmailListModelClass> call, Response<EmailListModelClass> response) {
                try {
                    Log.d(TAG+"Response",response.toString());
                    progress.setVisibility(View.GONE);
                    if (response.code() == 200) {
                        updateAfterSuccess(response.body());

                    } else if (response.code() == 400 || response.code() == 401 || response.code() == 405) {
                        Gson gson = new Gson();
                        Type type = (new TypeToken<EmailListModelClass>() {
                        }).getType();
                        EmailListModelClass var5 = gson.fromJson(response.errorBody().charStream(), type);
                    }
                } catch (JsonIOException e) {
                    e.printStackTrace();
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }

            }

            public void onFailure(Call<EmailListModelClass> call, Throwable t) {
                Log.e(TAG, Arrays.toString(t.getStackTrace()));
                progress.setVisibility(View.GONE);
            }
        });

    }

    private void updateAfterSuccess(EmailListModelClass emailListModelClass) {
        for (int i = 0; i < globalEmailList.size(); i++) {
            if (globalEmailList.get(i).getIdtableEmail() == emailListModelClass.getIdtableEmail()) {
                globalEmailList.set(i, emailListModelClass);
                emailListAdapter.notifyItemChanged(i);
            }
        }
    }


    private void initView() {
        addEmailButton = findViewById(R.id.addEmailButton);
        emailListRecyclerView = findViewById(R.id.emailrecycler_view);
        progress = findViewById(R.id.progress);
        emailListRecyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    private void initListener() {
        findViewById(R.id.addEmailButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.addEmailButton) {
            addEmail();
        }
    }

    private void addEmail() {
        showAlertDialog("Add Email", "SAVE", false, new CustomAlertDialogButtonsCallbackListener() {
            @Override
            public void onPositiveButtonClick(AppCompatDialog dialog, String email) {


                if (ValidationUtils.emailValidation((CustomEditText)dialog.findViewById(R.id.edittext_email))) {
                    dialog.dismiss();
                    addEmailToList(email);
                }
            }

            @Override
            public void onNegativeButtonClick(AppCompatDialog dialog) {
                dialog.dismiss();
            }
        });

    }

    private void addEmailToList(String email) {
        progress.setVisibility(View.VISIBLE);
        JSONObject jObject = new JSONObject();
        try {
            jObject.put("tableEmailEmailAddress", email);
            jObject.put("tableEmailValidate", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG+"Request",jObject.toString());
        RequestBody requestBody = RequestBody.create(MediaType.parse(Constants.APPLICATION_JSON), jObject.toString());
        ApiClient.getInstance(this).addEmail(requestBody).enqueue(new Callback<EmailListModelClass>() {
            public void onResponse(Call<EmailListModelClass> call, Response<EmailListModelClass> response) {
                try {
                    Log.d(TAG+"Response",response.toString());
                    progress.setVisibility(View.GONE);
                    if (response.code() == 200) {
                        addAfterSuccess(response.body());

                    } else if (response.code() == 400 || response.code() == 401 || response.code() == 405) {
                        Gson gson = new Gson();
                        Type type = (new TypeToken<EmailListModelClass>() {
                        }).getType();
                        EmailListModelClass var5 = gson.fromJson(response.errorBody().charStream(), type);
                    }
                } catch (JsonIOException e) {
                    e.printStackTrace();
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }

            }

            public void onFailure(Call<EmailListModelClass> call, Throwable t) {
                Log.e(TAG, Arrays.toString(t.getStackTrace()));
                progress.setVisibility(View.GONE);
            }
        });

    }

    private void addAfterSuccess(EmailListModelClass emailListModelClass) {
        globalEmailList.add(emailListModelClass);
        emailListAdapter.notifyDataSetChanged();
    }
}
