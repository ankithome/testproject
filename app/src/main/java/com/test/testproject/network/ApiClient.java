package com.test.testproject.network;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.test.testproject.utils.Constants;

import org.json.JSONObject;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by lenovo on 9/4/2018.
 */

public class ApiClient {

    public static TestServices getInstance(final Context context) {
        OkHttpClient okClient = new OkHttpClient.Builder()
                .addInterceptor(new CustomInterceptor(context))
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS)
                .writeTimeout(500, TimeUnit.SECONDS)
                .build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder().
                baseUrl(Constants.getBaseUrl()).
                addConverterFactory(GsonConverterFactory.create(gson))
                .client(getUnsafeOkHttpClient(context).build())
                .build();
        return retrofit.create(TestServices.class);
    }
    private static OkHttpClient.Builder getUnsafeOkHttpClient(final Context context) {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {

                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {

                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }

            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            OkHttpClient.Builder builder = new OkHttpClient.Builder()
                    .addInterceptor(new CustomInterceptor(context))
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(1, TimeUnit.MINUTES)
                    .writeTimeout(5, TimeUnit.MINUTES);
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            return builder;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static class CustomInterceptor implements Interceptor {
        Context context;

        CustomInterceptor(Context context) {
            this.context = context;
        }

        @Override
        public Response intercept(final Chain chain) throws IOException {
            final Request original = chain.request();
            // Request customization: add request headers
            Request.Builder requestBuilder = original.newBuilder()
                    .header("Content-Type", "application/json" )
                    .addHeader("Accept", "application/json")
                    .method(original.method(), original.body());
            final Request modifiedRequest = requestBuilder.build();

            Response response = chain.proceed(modifiedRequest);
            try {
                Object[] clones = cloneResponseBody(response);
                response = (Response) clones[1];
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        Object[] cloneResponseBody(Response response) {
            Object[] clones = new Object[2];
            try {
                MediaType contentType = null;
                String bodyString = "";
                if (response.body() != null) {
                    contentType = response.body().contentType();
                    bodyString = response.body().string();
                }

                ResponseBody body = ResponseBody.create(contentType, bodyString);
                ResponseBody body1 = ResponseBody.create(contentType, bodyString);
                response = response.newBuilder().body(body1).build();

                clones[0] = body;
                clones[1] = response;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return clones;
        }
    }
}
