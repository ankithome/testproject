package com.test.testproject.network;

import com.test.testproject.model.EmailListModelClass;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface TestServices {

    @GET("wmsweb/webapi/email/")
    Call<ArrayList<EmailListModelClass>> getEmailList();

    @DELETE("wmsweb/webapi/email/{idtableEmail}")
    Call<Boolean> deleteEmail(@Path("idtableEmail") int idtableEmail);

    @PUT("wmsweb/webapi/email/{idtableEmail}")
    Call<EmailListModelClass> updateEmail(@Path("idtableEmail") int idtableEmail, @Body RequestBody body);

    @POST("wmsweb/webapi/email/")
    Call<EmailListModelClass> addEmail(@Body RequestBody body);

}
