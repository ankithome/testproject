package com.test.testproject.model;

public class EmailListModelClass {
    private int idtableEmail;
    private String tableEmailEmailAddress;
    private boolean tableEmailValidate;

    public int getIdtableEmail() {
        return idtableEmail;
    }

    public void setIdtableEmail(int idtableEmail) {
        this.idtableEmail = idtableEmail;
    }

    public String getTableEmailEmailAddress() {
        return tableEmailEmailAddress;
    }

    public void setTableEmailEmailAddress(String tableEmailEmailAddress) {
        this.tableEmailEmailAddress = tableEmailEmailAddress;
    }

    public boolean isTableEmailValidate() {
        return tableEmailValidate;
    }

    public void setTableEmailValidate(boolean tableEmailValidate) {
        this.tableEmailValidate = tableEmailValidate;
    }
}
