package com.test.testproject.utils;

import android.widget.EditText;

import com.test.testproject.R;

public class ValidationUtils {
    static String emailRegex = "[a-zA-Z0-9._-]+@[a-zA-Z0-9]+\\.+[a-zA-Z0-9]+";

    public static boolean emailValidation(EditText editText) {
        String emailString = editText.getText().toString().trim();
        if (emailString.matches(emailRegex) && emailString.length() > 0) {
            return true;
        } else {
            editText.setError(editText.getContext().getString(R.string.error_email_invalid));
            return false;
        }
    }
}
