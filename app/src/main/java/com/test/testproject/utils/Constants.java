package com.test.testproject.utils;

public class Constants {

    public static final String APPLICATION_JSON = "application/json";

    public static String getBaseUrl() {
        return "https://devfrontend.gscmaven.com/";
    }

}
