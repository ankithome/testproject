package com.test.testproject.utils;

import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.LinearLayout;

import com.an.customfontview.CustomEditText;
import com.an.customfontview.CustomTextView;
import com.test.testproject.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialog;

public class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    public void onClick(View v) {

    }

    public interface CustomAlertDialogButtonsCallbackListener {
        void onPositiveButtonClick(AppCompatDialog dialog, String emailId);

        void onNegativeButtonClick(AppCompatDialog dialog);
    }

    public void showAlertDialog(String message,String buttonText,
                                boolean isOnlyPositiveButton,
                                final CustomAlertDialogButtonsCallbackListener listener) {
        try {
            final AppCompatDialog dialog = new AppCompatDialog(this/*, R.style.DialogCustomTheme*/);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.layout_custom_alert_dialog_cancel);
            dialog.setCancelable(false);

            final CustomTextView text_msg = dialog.findViewById(R.id.text_msg);
            text_msg.setText(message);
            LinearLayout text_no = dialog.findViewById(R.id.text_no);
            final CustomEditText edittext_email = dialog.findViewById(R.id.edittext_email);
            CustomTextView text_yes = dialog.findViewById(R.id.text_yes);
            text_yes.setText(buttonText);
            if (!isOnlyPositiveButton) {
                text_no.setVisibility(View.VISIBLE);
                text_yes.setVisibility(View.VISIBLE);
                text_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.onNegativeButtonClick(dialog);
                        } else {
                            dialog.cancel();
                        }
                    }
                });
            } else {
                text_no.setVisibility(View.GONE);
            }
            text_yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onPositiveButtonClick(dialog, edittext_email.getText().toString());
                    } else {
                        dialog.cancel();
                    }
                }
            });
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDeleteAlertDialog(String email,
                                      boolean isOnlyPositiveButton,
                                      final CustomAlertDialogButtonsCallbackListener listener) {
        try {
            final AppCompatDialog dialog = new AppCompatDialog(this/*, R.style.DialogCustomTheme*/);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.delete_alert_dialog);
            dialog.setCancelable(false);

            final CustomTextView text_msg = dialog.findViewById(R.id.text_msg);
            text_msg.setText("Delete Email");
            CustomTextView text_no = dialog.findViewById(R.id.text_no_button);
            final CustomTextView edittext_email = dialog.findViewById(R.id.edittext_email);
            edittext_email.setText(email);
            CustomTextView text_yes = dialog.findViewById(R.id.text_yes_button);
            if (!isOnlyPositiveButton) {
                text_no.setVisibility(View.VISIBLE);
                text_yes.setVisibility(View.VISIBLE);
                text_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.onNegativeButtonClick(dialog);
                        } else {
                            dialog.cancel();
                        }
                    }
                });
            } else {
                text_no.setVisibility(View.GONE);
            }
            text_yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onPositiveButtonClick(dialog, edittext_email.getText().toString());
                    } else {
                        dialog.cancel();
                    }
                }
            });
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
