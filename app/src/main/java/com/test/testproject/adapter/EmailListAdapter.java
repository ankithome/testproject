package com.test.testproject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.an.customfontview.CustomTextView;
import com.test.testproject.R;
import com.test.testproject.model.EmailListModelClass;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class EmailListAdapter extends RecyclerView.Adapter<EmailListAdapter.EmailListViewHolder> {

    private ArrayList<EmailListModelClass> emailLists;
    private OnEditDeleteClickListner onEditDeleteClickListner;

    public EmailListAdapter(ArrayList<EmailListModelClass> emailLists,OnEditDeleteClickListner onEditDeleteClickListner) {
        this.emailLists = emailLists;
        this.onEditDeleteClickListner = onEditDeleteClickListner;
    }

    public interface OnEditDeleteClickListner {
        void onEdit(EmailListModelClass emailListModelClass);

        void onDelete(EmailListModelClass emailListModelClass);
    }

    @NonNull
    @Override
    public EmailListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.adapter_email_list, parent, false);
        return new EmailListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmailListViewHolder holder, int position) {
        final EmailListModelClass emailListData = emailLists.get(position);
        holder.serial_no.setText("" + emailListData.getIdtableEmail());
        holder.email_address.setText(emailListData.getTableEmailEmailAddress());
        holder.edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEditDeleteClickListner != null) {
                    onEditDeleteClickListner.onEdit(emailListData);
                }
            }
        });
        holder.delete_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onEditDeleteClickListner != null) {
                    onEditDeleteClickListner.onDelete(emailListData);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return emailLists.size();
    }

    static class EmailListViewHolder extends RecyclerView.ViewHolder {
        CustomTextView serial_no, email_address;
        ImageView edit_btn, delete_btn;

        EmailListViewHolder(@NonNull View itemView) {
            super(itemView);
            edit_btn = itemView.findViewById(R.id.edit_btn);
            delete_btn = itemView.findViewById(R.id.delete_btn);
            serial_no = itemView.findViewById(R.id.serial_no);
            email_address = itemView.findViewById(R.id.email_address);

        }
    }
}
